#!/usr/bin/env python3
'''
Analyze the IMDB movie metadata

Jackson Waller, June 2018
'''

import csv
import os
import numpy
import pandas
import peewee

db = peewee.SqliteDatabase('im.db', pragmas={'foreign_keys': 'on'})
FILE = './movie_metadata.csv'

##### CLASSES #####

class BaseModel(peewee.Model):
    """ Common to all models """
    class Meta:
        database = db


class Director(BaseModel):
    """
    Director of films, e.g. Joss Whedon
    """
    name = peewee.CharField()


class Movie(BaseModel):
    """
    A film, e.g. 'The Avengers'
    """
    title = peewee.CharField()
    budget = peewee.IntegerField()
    gross = peewee.IntegerField()
    director = peewee.ForeignKeyField(Director, backref='movies')
    profitability = peewee.FloatField()
    score = peewee.FloatField()
    user_reviews = peewee.IntegerField()
    critic_reviews = peewee.IntegerField()
    votes = peewee.IntegerField()


class Actor(BaseModel):
    """
    An actor in films, e.g. Robert Downey Jr.
    """
    name = peewee.CharField()


class Genre(BaseModel):
    """
    A movie genre, e.g. Action
    """
    name = peewee.CharField()


class ActorToMovie(BaseModel):
    """
    Junction table to relate actors to movies
    """
    actor = peewee.ForeignKeyField(Actor, backref='movies')
    movie = peewee.ForeignKeyField(Movie, backref='actors')


class GenreToMovie(BaseModel):
    """
    Junction table to relate genres to movies
    """
    genre = peewee.ForeignKeyField(Genre, backref='movies')
    movie = peewee.ForeignKeyField(Movie, backref='genres')


##### PARSING FUNCTIONS ##### 

def parse_row(row):
    """
    Parse a line into the database
    """

    # Uses more memory, but simpler to add here than calculate on the fly
    profitability = (float(row['gross']) - float(row['budget'])) / float(row['budget'])

    movie, _ = Movie.get_or_create(
        # All titles end with non-breaking spaces that complicate queries
        title=row['movie_title'].replace('\xa0', ''),
        director=Director.get_or_create(
            name=row['director_name']
        )[0],
        gross=row['gross'],
        budget=row['budget'],
        score=row['imdb_score'],
        user_reviews=row['num_user_for_reviews'],
        critic_reviews=row['num_critic_for_reviews'],
        votes=row['num_voted_users'],
        profitability=profitability
    )

    # Populate junction tables
    for this_actor in (row['actor_1_name'], row['actor_2_name'], row['actor_3_name']):
        actor, _ = Actor.get_or_create(name=this_actor)
        actor_relation, _ = ActorToMovie.get_or_create(
            actor=actor,
            movie=movie
        )

    for this_genre in row['genres'].split('|'):
        genre, _ = Genre.get_or_create(name=this_genre)
        genre_relation, _ = GenreToMovie.get_or_create(
            genre=genre,
            movie=movie
        )


def parse_file():
    """
    Parse the data from the CSV file and insert it into the database, if necessary
    """
    necessary = ['gross', 'budget', 'num_critic_for_reviews',
                      'num_user_for_reviews', 'num_voted_users', 'imdb_score']

    with open(FILE) as f:
        data = csv.DictReader(f)

        with db.atomic():

            skipped = 0
            for row in data:

                # skip rows with missing data
                # there are more graceful ways, but rows missing these values are useless for our purposes
                # we particularly don't want to have a falsey budget
                if any((row[i] == '' for i in necessary)):
                    skipped += 1
                    continue

                parse_row(row)

    return skipped


##### ANALYSIS FUNCTIONS ##### 

def top_ten(series):
    """
    Return the ten largest values in a series
    """
    return series.sort_values(ascending=False)[:10]


def many_to_many_query(primary, secondary, junction):
    """
    Return the average profitiability of an object
    in a many-to-many relationship

    param primary: the table to group by
    param secondary: the table whose profitability to average
    """
    query = (junction
                .select(junction, primary, secondary)
                .join(secondary)
                .switch(junction)
                .join(primary)
                .group_by(primary)
    )

    return query


def get_genre_profitabilities():
    """
    Return the profitability of each genre
    Define genre profitability as the average profitability of all movies in the genre
    """
    # Find the movies in each genre
    query = many_to_many_query(Genre, Movie, GenreToMovie)

    # Average the profitability of all movies in each genre
    # There is probably a way to create a dataframe directly from the query
    d = dict()
    for genre_movie in query:
        d[genre_movie.genre.name] = numpy.average(
            # numpy.average doesn't like generators
            tuple(genre_movie_alias.movie.profitability for genre_movie_alias in genre_movie.genre.movies)
            )

    genre_profitabilities = pandas.Series(d)

    return genre_profitabilities


def get_actor_profitabilities():
    """
    Return the average profitability of each actor 
    """

    # Find the movies by each actor
    query = many_to_many_query(Actor, Movie, ActorToMovie)

    d = dict()
    for actor_movie in query:
        d[actor_movie.actor.name] = numpy.average(
            tuple(actor_movie_alias.movie.profitability for actor_movie_alias in actor_movie.actor.movies)
        )

    actor_profitabilities = pandas.Series(d)

    return actor_profitabilities


def get_director_profitabilities():
    """
    Return the average profitability of each director
    """

    # Find the movies by each director
    query = (Director
                .select(Director, Movie.profitability)
                .join(Movie)
                .group_by(Director)
    )

    d = dict()
    for director in query:
        d[director.name] = numpy.average(
            tuple(movie.profitability for movie in director.movies)
        )

    director_profitabilities = pandas.Series(d)

    return director_profitabilities


def get_most_profitable_people():
    """
    Return the top ten most profitable actors or directors
    """
    people_profitabilities = pandas.concat(
        (get_actor_profitabilities(), get_director_profitabilities())
    )

    return top_ten(people_profitabilities)


def get_movie_covariance_matrix():
    """
    Return the covariance matrix
    Or, do good movies make money?
    """

    movie_data = []
    for movie in Movie.select():
        movie_data.append({
            'profitability': movie.profitability,
            'score': movie.score,
            'user_reviews': movie.user_reviews,
            'critic_reviews': movie.critic_reviews,
            'votes': movie.votes
        })

    df = pandas.DataFrame(movie_data)
    covariance = df.cov()

    return covariance

def main():

    if not os.path.isfile('im.db'):
        db.connect()
        db.create_tables([Director, Movie, Actor, ActorToMovie, Genre, GenreToMovie])
        skipped = parse_file()
        print(skipped)

    else:
        db.connect()


    print("\nTop ten most profitable genres:\n",
            top_ten(get_genre_profitabilities())
    )

    print("\n----------------\n\n",
            "Top ten most profitable actors or directors:\n",
            top_ten(get_most_profitable_people())
    )

    print("\n----------------\n\n",
            "Covariance matrix of movie attributes:\n",
            get_movie_covariance_matrix()
        )

if __name__ == '__main__':
    main()