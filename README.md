IMDB Playground
===============

Overview
--------

This project implements some simple Python functions to create a small database
and analyze some IMDB movie metadata. See requirements.txt for necessary
libraries.

Running
-------

Run main.py with Python 3 to see the analysis in action, or run tests.py to
validate some of the functionality.

Analysis Steps
--------------

1. Creating the database

    The provided data is organized around movies, but each movie has several
    actors and genres, forming many-to-many relationships. The director of a
    given movie may have also directed other movies. This means that in
    addition to the movies, I need to create tables for actors, genres and
    directors. In addition, I need to create junction tables for each of
    the many-to-many relationships. These junction tables allow me to query
    multi-valued objects such as all of the movies that Johnny Depp starred
    in, or every actor in "The Avengers".

2. Parsing the csv file

    I chose to use the standard csv module to read the file due to its
    simplicty and flexibility. The data for some movies were incomplete, so I
    simply chose to ignore rows that were missing certain important values. A
    more rigorous analysis might require something more graceful, but rows
    missing important values like 'gross' or 'budget' are effectively useless
    here.

3. Finding most profitable objects

    To find the most profitable genres I first needed to join the Genre table
    with the Movie table, which I did using the ActorToMovie junction table
    I created earlier. I then use the resulting object to group movies by
    their genre. This allows me to create a list of movies for each genre
    which I can take an average over.

    I believe that this entire step can be completed with a single
    well-constructed SQL query, but in the interest of time (and due to my
    own inexperience) I chose to load the resulting genre profitability data
    into a Pandas series for simple analysis. This allowed me to easily sort
    the data and slice the sorted series to choose the ten largest values.

    The analysis behind the most profitable actors or directors was identical,
    except that it was necessary to combine the profitability of the actors
    with that of the directors. Again, inserting the data into the pandas
    series made this easy.

4. Other questions

    I like movies very much, and so I am interested in whether a movie's
    "quality" (as determined by its IMDB score) is related to its
    profitability. In order to investigate this I created a covariance matrix
    for several variables contained in the movie data. Unfortunately
    profitability and score were very weakly related under the assumptions I
    used. However, several of the variables I chose had relationships that
    would be obvious with common sense; for instance, a movie with more user
    reviews is likely to be more profitable because many of those people
    probably bought movie tickets. Taking other data such as the release year
    into account could be more useful.


