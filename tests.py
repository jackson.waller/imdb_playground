"""
A basic testing framework
"""

import unittest
import main

class MovieDataTest(unittest.TestCase):

    # For serious development we may want to iterate these tests over each set of similar functions
    def test_highest(self):
        """
        Make sure our measure of the most profitable actor matches a simpler measure
        """
        actor_profitabilities = main.get_actor_profitabilities()
        highest_actor_profitabilities = main.top_ten(actor_profitabilities)
        self.assertEqual(highest_actor_profitabilities[0], actor_profitabilities.max())


    def test_series_completeness(self):
        """
        Make sure that our series didn't miss anything
        """
        director_profitabilities = main.get_director_profitabilities()
        all_directors = main.Director.select()
        missed = (director.name not in director_profitabilities.index for director in all_directors)
        self.assertIs(any(missed), False)


    def test_many_to_many_query(self):
        """
        Test that our many-to-many query returns properly
        """
        query = main.many_to_many_query(main.Actor, main.Movie, main.ActorToMovie)
        self.assertTrue(type(query[0].actor.name)==str)

    def test_covariance(self):
        """
        Test that we have a valid covariance matrix by checking the symmetry
        """
        cov_matrix = main.get_movie_covariance_matrix()
        self.assertIs(all(cov_matrix == cov_matrix.T), True)

if __name__ == "__main__":
    unittest.main()
